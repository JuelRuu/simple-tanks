#include "TankAIController.h"
#include "Tank.h"

void ATankAIController::BeginPlay() {
	Super::BeginPlay();
}

void ATankAIController::SetPawn(APawn* InPawn){
	Super::SetPawn(InPawn);

	if(InPawn){
		auto PossessTank = Cast<ATank>(InPawn);
		if(!ensure(PossessTank)){ return; }
		PossessTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnPossedTankDeath);

	}
}

void ATankAIController::OnPossedTankDeath(){
	UE_LOG(LogTemp, Warning, TEXT("Received!"));
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto PlayerTank = Cast<ATank>(GetWorld()->GetFirstPlayerController()->GetPawn());
	auto ControlledTank = Cast<ATank>(GetPawn());

	if (PlayerTank) {
		MoveToActor(PlayerTank, AcceptanceRadius);
		ControlledTank->AimAt(PlayerTank->GetActorLocation());
		ControlledTank->Fire();
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("Player not found"));
}
